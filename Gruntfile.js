'use strict';

module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    clean: {
      output: ['./www/assets/*']
    },
    jshint: {
      options: {
        force: true
      },
      files: ['./www/js/**/*.js']
    },
    concat: {
      css_app: {
        dest: './www/assets/app.css',
        src: [
          './www/lib/ionic/css/ionic.css',
          './www/css/**/*.css'
        ]
      },
      js_app: {
        options: {
          separator: ';'
        },
        dest: './www/assets/app.js',
        src: [
          './www/lib/ionic/js/ionic.bundle.js',
          './www/lib/ionic/js/angular/angular-resource.js',
          './www/js/**/*.js'
        ]
      }
    },
    cssmin: {
      app: {
        src: './www/assets/app.css',
        dest: './www/assets/app.min.css'
      }
    },
    ngAnnotate: {
      options: {
        singleQuotes: true
      },
      app: {
        files: {
          './www/assets/app.js': ['./www/assets/app.js']
        }
      }
    },
    uglify: {
      app: {
        files: {
          './www/assets/app.js': ['./www/assets/app.js']
        }
      },
      options: {

      }
    },
    htmlhint: {
      templates: {
        options: {
          'attr-lower-case': true,
          'attr-value-not-empty': true,
          'tag-pair': true,
          'tag-self-close': true,
          'tagname-lowercase': true
        },
        src: ['./www/templates/**/*.html']
      }
    },
    htmlmin: {
      dev: {
        options: {
          removeEmptyAttributes: true,
          removeEmptyElements: true,
          removeRedundantAttributes: true,
          removeComments: true,
          removeOptionalTags: true,
          collapseWhitespace: true
        },
        files: [{
          expand: true,
          cwd: './www/templates/',
          dest: './www/assets/templates/',
          src:  ['*.html'],
          ext: ".min.html",
          extDot: 'last'
        }]
      }
    }
  });

  grunt.loadNpmTasks("grunt-contrib-clean");
  grunt.loadNpmTasks("grunt-contrib-concat");
  grunt.loadNpmTasks("grunt-contrib-jshint");
  grunt.loadNpmTasks("grunt-contrib-uglify");
  grunt.loadNpmTasks("grunt-ng-annotate");
  grunt.loadNpmTasks("grunt-css")
  grunt.loadNpmTasks("grunt-htmlhint");
  grunt.loadNpmTasks("grunt-contrib-htmlmin");

  grunt.registerTask("default", ['clean']);
  grunt.registerTask("minify", ['clean', 'concat', 'cssmin', 'ngAnnotate', 'uglify']);
};