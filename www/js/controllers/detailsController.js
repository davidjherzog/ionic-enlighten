(function () {
  'use strict';

  angular
    .module('enlighten')
    .controller('DetailsCtrl', DetailsCtrl);

  DetailsCtrl.$inject = ['enlightenSummary', 'enlightenStats', 'enlightenUtils'];

  function DetailsCtrl(enlightenSummary, enlightenStats, enlightenUtils) {

    var vm = this;
    var currentDate = "";
    var selectedDate = "";
    var start = "";
    var end = "";
    var offset = "";
    var watts = [];
    var time = [];
    var width = 320;

    loadSummary();

    vm.loadSummary = loadSummary;
    vm.loadStats = loadStats;
    vm.changeDate = changeDate;

    //vm.date = new Date().toDateInputValue();
    vm.date = new Date();
    vm.minDate = new Date().addMonths(-6).toDateInputValue();
    vm.maxDate = vm.date.toDateInputValue();
    vm.beginDay = null;
    vm.endDay = null;

    // Summary
    function loadSummary() {
      // todo - system id is hard coded - need to get from selected system in menu list
      enlightenSummary.getSummary('15180').$promise.then(loadSummarySuccess, loadSummaryFailure);
    }

    function loadSummarySuccess(data) {
      vm.summary = {};
      vm.summary.current_power = enlightenUtils.calculateKiloWattHours(data.current_power);
      vm.summary.energy_today = enlightenUtils.calculateKiloWattHours(data.energy_today);
      vm.summary.energy_week = enlightenUtils.calculateKiloWattHours(data.energy_week);
      vm.summary.energy_month = enlightenUtils.calculateKiloWattHours(data.energy_month);
      vm.summary.energy_lifetime = enlightenUtils.calculateKiloWattHours(data.energy_lifetime);
      vm.summary.modules = data.modules;
      console.log(data);
    }

    function loadSummaryFailure(data) {
      vm.summary = {};
      console.log('Failed to load summary: ' + data.status);
    }

    // Stats
    function loadStats() {
      // todo - system id is hard coded - need to get from selected system in menu list
      enlightenStats.getStats('15180', vm.beginDay, vm.endDay).$promise.then(loadStatsSuccess, loadStatsFailure);
    }

    function loadStatsSuccess(data) {

      // todo - need to actually do something with the data and present it in the UI
      vm.stats = {};

      var intervals = data.intervals;
      var listItems = '';
      var previousHour = '';
      var count = 0;
      var listItem = '';
      var divider = '';
      var totalPower = 0;
      var totalWatts = 0;

      var watts = new Array(intervals.length);
      var time = new Array(intervals.length);

      var display = [];
      var reporting = [];

      for (var i = intervals.length - 1; i > -1; i--) {

        var hour = intervals[i].end_date.substring(11, 13);
        if ((hour != previousHour) && (previousHour != '')) {
          var divider = {
            time: enlightenUtils.formatHourlyTime(previousHour),
            count: count,
            intervals: reporting
          }
          display.push(divider);
          count = 0;
          listItem = '';
          reporting = [];
        }
        previousHour = hour;
        count++;

        totalPower = totalPower + intervals[i].powr;
        totalWatts = totalWatts + intervals[i].enwh;

        var formattedTime = enlightenUtils.formatTime(intervals[i].end_date.substring(11, 16));
        watts[i] = intervals[i].enwh;
        time[i] = formattedTime;

        var reportedInterval = {
          devices: intervals[i].devices_reporting,
          endDate: formattedTime,
          powr: enlightenUtils.calculateKiloWattHours(intervals[i].powr),
          enwh: enlightenUtils.calculateKiloWattHours(intervals[i].enwh)
        };
        reporting.push(reportedInterval);

      }
      ;
      if (intervals.length == 0) {
        //listItems = '<li>No Stats</li>'
      } else {
        vm.totalPower = enlightenUtils.calculateKiloWattHours(totalPower);
        vm.totalEnergy = enlightenUtils.calculateKiloWattHours(totalWatts);
        vm.dividers = display;
      }

      $("#sparkline").css("width", width + 'px');
      $("#sparkline").sparkline(watts, {
        type: 'line',
        width: width + 'px',
        height: '40px',
        fillColor: '#0000f0',
        spotColor: '#0000f0',
        minSpotColor: '#0000f0',
        maxSpotColor: '#0000f0',
        highlightSpotColor: '#0000f0',
        highlightLineColor: '#0000f0',
        enableTagOptions: false
      });

    }

    function loadStatsFailure(data) {
      vm.summary = {};
      console.log('Failed to load stats: ' + data.status);
    }

    function changeDate() {
      var dates = formateDate(vm.date);
      vm.beginDay = dates.start;
      vm.endDay = dates.end;
      loadStats();
    }

    function checkLength(value) {
      console.log('value: ' + value);
      if (value < 10) {
        console.log('value1: ' + value);
        return "0" + value;
      } else {
        console.log('value2: ' + value);
        return value;
      }
    }

    function formateDate(date) {

      var day = checkLength(date.getDate());
      var month = checkLength(date.getMonth() + 1);
      var year = date.getFullYear();
      console.log('day: ' + day);
      console.log('month: ' + month);

      var offset = date.getTimezoneOffset() / 60;
      if (offset < 10) {
        offset = "0" + offset + ":00";
      } else {
        offset = offset + ":00";
      }

      var dates = {
        'start': start = year + "-" + month + "-" + day + "T00:01-" + offset,
        'end': end = year + "-" + month + "-" + day + "T23:59-" + offset
      };

      return dates;
    }

  }

})();
