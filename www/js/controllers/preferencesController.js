(function() {
  'use strict';

  angular
    .module('enlighten')
    .controller('PreferencesCtrl', PreferencesCtrl);

    PreferencesCtrl.$inject = ['$location', '$ionicLoading'];

    function PreferencesCtrl($location, $ionicLoading) {

      var vm = this;

      vm.save = save;

      function save() {
        $location.path('app');
      }

      //$ionicLoading.show({
      //  template: 'Loading...'
      //});

    }

})();
