(function() {
  'use strict';

  angular
    .module('enlighten')
    .controller('AppCtrl', AppCtrl);

    AppCtrl.$inject = ['$scope', '$ionicModal', 'enlightenSystems', 'enlightenStorage'];

    function AppCtrl($scope, $ionicModal, enlightenSystems, enlightenStorage) {

      var menuView = this;

      loadSystems();

      menuView.loadSystems = loadSystems;

      function loadSystems() {
        enlightenSystems.getSystems().$promise.then(loadSystemsSuccess, loadSystemsFailure);
      }

      function loadSystemsSuccess(data) {
        menuView.systems = data.systems;
      }

      function loadSystemsFailure(data) {
        menuView.systems = {};
      }

      // Form data for the preferences modal
      $scope.prefs = {
        url: enlightenStorage.getUrl(),
        apiKey: enlightenStorage.getApiKey()
      };

      // Create the preferences modal that we will use later
      $ionicModal.fromTemplateUrl('templates/preferences.html', {
        scope: $scope
      }).then(function(modal) {
        $scope.modal = modal;
      });

      // Triggered in the preferences modal to close it
      $scope.close = function() {
        $scope.modal.hide();
      };

      // Open the preferences modal
      $scope.preferences = function() {
        $scope.modal.show();
      };

      // Perform the preferences action when the user submits the preferences form
      $scope.save = function() {
        console.log('Saving Preferences - URL: ', $scope.prefs.url);
        console.log('Saving Preferences - APIKey: ', $scope.prefs.apiKey);
        enlightenStorage.setUrl($scope.prefs.url);
        enlightenStorage.setApiKey($scope.prefs.apiKey);

        $scope.close();
      };

      // Create the help modal that we will use later
      $ionicModal.fromTemplateUrl('templates/help.html', {
        scope: $scope
      }).then(function(modal) {
        $scope.modalHelp = modal;
      });

      // Triggered in the preferences modal to close it
      $scope.closeHelp = function() {
        $scope.modalHelp.hide();
      };

      // Open the preferences modal
      $scope.help = function() {
        $scope.modalHelp.show();
      };

    }

})();
