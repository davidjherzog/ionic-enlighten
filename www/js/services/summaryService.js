(function() {
  'use strict';

  angular
    .module('enlighten')
    .factory('enlightenSummary', enlightenSummary);

    enlightenSummary.$inject = ['enlightenResource'];

    function enlightenSummary(enlightenResource) {

      var summary ={
        getSummary: getSummary
      };
      return summary;

      function getSummary(systemId) {
        var params = {
          systemId: systemId
        };
        return enlightenResource.query('/systems/:systemId/summary', params);
      }

    }

})();
