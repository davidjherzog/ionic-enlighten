(function() {
  'use strict';

  angular
    .module('enlighten')
    .factory('enlightenSystems', enlightenSystems);

    enlightenSystems.$inject = ['enlightenResource'];

    function enlightenSystems(enlightenResource) {

      var systems ={
        getSystems: getSystems
      };
      return systems;

      function getSystems() {
        console.log('calling enlightenSystems.getSystems...');
        return enlightenResource.query('/systems', {});
      }

    }

})();
