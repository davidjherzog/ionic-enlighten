(function() {
  'use strict';

  angular
    .module('enlighten')
    .factory('enlightenStorage', enlightenStorage);

    function enlightenStorage($window) {

      var storage = {
        getDb: getDb,
        getUrl: getUrl,
        getApiKey: getApiKey,
        setUrl: setUrl,
        setApiKey: setApiKey
      };
      return storage;

      function getDb() {
        try {
          if( !! $window.localStorage ) return $window.localStorage;
        } catch(e) {
          return undefined;
        }
      }

      function getUrl() {
        return storage.getDb().getItem("url");
      }

      function getApiKey() {
        return storage.getDb().getItem("apiKey");
      }

      function setUrl(url) {
        storage.getDb().setItem("url", url);
      }

      function setApiKey(apiKey) {
        storage.getDb().setItem("apiKey", apiKey);
      }

    }

})();
