(function() {
  'use strict';

  angular
    .module('enlighten')
    .factory('enlightenUtils', enlightenUtils);

    function enlightenUtils() {

      var utils = {
        getParameter: getParameter,
        calculateKiloWattHours: calculateKiloWattHours,
        format: format,
        formatNumber: formatNumber,
        formatHourlyTime: formatHourlyTime,
        formatTime: formatTime
      };
      return utils;

			function getParameter(name) {
				name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
				var regexS = "[\\?&]" + name + "=([^&#]*)";
				var regex = new RegExp(regexS);
				var results = regex.exec(window.location.href);
				if (results === null)
					return "";
				else
					return results[1];
			}

			function calculateKiloWattHours(wattHours) {
				if (wattHours < 1000) {
					return wattHours + " Wh";
				} else if (wattHours < 1000000) {
					return this.format(wattHours / 1000, 2) + " kWh";
				} else {
					return this.format(wattHours / 1000000, 2) + " MWh";
				}
			}

			function format(pnumber, decimals) {
				if (isNaN(pnumber)) { return 0};
				if (pnumber=='') { return 0};
				var snum = new String(pnumber);
				var sec = snum.split('.');
				var whole = parseFloat(sec[0]);
				var result = '';
				if(sec.length > 1){
					var dec = new String(sec[1]);
					dec = String(parseFloat(sec[1])/Math.pow(10,(dec.length - decimals)));
					dec = String(whole + Math.round(parseFloat(dec))/Math.pow(10,decimals));
					var dot = dec.indexOf('.');
					if(dot == -1){
						dec += '.';
						dot = dec.indexOf('.');
					}
					while(dec.length <= dot + decimals) { dec += '0'; }
					result = dec;
				} else{
					var dot;
					var dec = new String(whole);
					dec += '.';
					dot = dec.indexOf('.');
					while(dec.length <= dot + decimals) { dec += '0'; }
					result = dec;
				}
				return result;
			}

			function formatNumber(obj, decimal) {
				//decimal  - the number of decimals after the digit from 0 to 3
				//-- Returns the passed number as a string in the xxx,xxx.xx format.
				anynum = eval(obj.value);
				divider = 10;
				switch(decimal) {
					case 0:
						divider = 1;
						break;
					case 1:
						divider = 10;
						break;
					case 2:
						divider = 100;
						break;
					default:
						//for 3 decimal places
						divider = 1000;
				}
				workNum = Math.abs((Math.round(anynum * divider) / divider));
				workStr = "" + workNum;

				if(workStr.indexOf(".") == -1) {
					workStr += ".";
				}
				dStr = workStr.substr(0, workStr.indexOf("."));
				dNum = dStr - 0;
				pStr = workStr.substr(workStr.indexOf("."));

				while(pStr.length - 1 < decimal) {
					pStr += "0";
				}

				if(pStr == '.')
					pStr = '';

				//--- Adds a comma in the thousands place.
				if(dNum >= 1000) {
					dLen = dStr.length;
					dStr = parseInt("" + (dNum / 1000)) + "," + dStr.substring(dLen - 3, dLen);
				}

				//-- Adds a comma in the millions place.
				if(dNum >= 1000000) {
					dLen = dStr.length;
					dStr = parseInt("" + (dNum / 1000000)) + "," + dStr.substring(dLen - 7, dLen);
				}
				retval = dStr + pStr;
				//-- Put numbers in parentheses if negative.
				if(anynum < 0) {
					retval = "(" + retval + ")";
				}

				//You could include a dollar sign in the return value.
				//retval =  "$"+retval
				obj.value = retval;
			}

			function formatHourlyTime(hour) {
				if (hour == '00') {
					return '12 AM';
				} else if (hour == '01') {
					return '1 AM';
				} else if (hour == '02') {
					return '2 AM';
				} else if (hour == '03') {
					return '3 AM';
				} else if (hour == '04') {
					return '4 AM';
				} else if (hour == '05') {
					return '5 AM';
				} else if (hour == '06') {
					return '6 AM';
				} else if (hour == '07') {
					return '7 AM';
				} else if (hour == '08') {
					return '8 AM';
				} else if (hour == '09') {
					return '9 AM';
				} else if (hour == '10') {
					return '10 AM';
				} else if (hour == '11') {
					return '11 AM';
				} else if (hour == '12') {
					return '12 PM';
				} else if (hour == '13') {
					return '1 PM';
				} else if (hour == '14') {
					return '2 PM';
				} else if (hour == '15') {
					return '3 PM';
				} else if (hour == '16') {
					return '4 PM';
				} else if (hour == '17') {
					return '5 PM';
				} else if (hour == '18') {
					return '6 PM';
				} else if (hour == '19') {
					return '7 PM';
				} else if (hour == '20') {
					return '8 PM';
				} else if (hour == '21') {
					return '9 PM';
				} else if (hour == '22') {
					return '10 PM';
				} else if (hour == '23') {
					return '11 PM';
				}
			}

			function formatTime(time) {
				var hour = time.substring(0,2);
				var amPm = '';

				if (hour == '00') {
					hour = '12';
					amPm = 'AM';
				} else if (hour == '01') {
					hour = '1';
					amPm = 'AM';
				} else if (hour == '02') {
					hour = '2';
					amPm = 'AM';
				} else if (hour == '03') {
					hour = '3';
					amPm = 'AM';
				} else if (hour == '04') {
					hour = '4';
					amPm = 'AM';
				} else if (hour == '05') {
					hour = '5';
					amPm = 'AM';
				} else if (hour == '06') {
					hour = '6';
					amPm = 'AM';
				} else if (hour == '07') {
					hour = '7';
					amPm = 'AM';
				} else if (hour == '08') {
					hour = '8';
					amPm = 'AM';
				} else if (hour == '09') {
					hour = '9';
					amPm = 'AM';
				} else if (hour == '10') {
					hour = '10';
					amPm = 'AM';
				} else if (hour == '11') {
					hour = '11';
					amPm = 'AM';
				} else if (hour == '12') {
					hour = '12';
					amPm = 'PM';
				} else if (hour == '13') {
					hour = '1';
					amPm = 'PM';
				} else if (hour == '14') {
					hour = '2';
					amPm = 'PM';
				} else if (hour == '15') {
					hour = '3';
					amPm = 'PM';
				} else if (hour == '16') {
					hour = '4';
					amPm = 'PM';
				} else if (hour == '17') {
					hour = '5';
					amPm = 'PM';
				} else if (hour == '18') {
					hour = '6';
					amPm = 'PM';
				} else if (hour == '19') {
					hour = '7';
					amPm = 'PM';
				} else if (hour == '20') {
					hour = '8';
					amPm = 'PM';
				} else if (hour == '21') {
					hour = '9';
					amPm = 'PM';
				} else if (hour == '22') {
					hour = '10';
					amPm = 'PM';
				} else if (hour == '23') {
					hour = '11';
					amPm = 'PM';
				}

				return hour + ':' + time.substring(3,5) + ' ' + amPm;
			}

    }

})();
