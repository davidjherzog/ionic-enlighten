(function() {
  'use strict';

  angular
    .module('enlighten')
    .factory('enlightenStats', enlightenStats);

    enlightenStats.$inject = ['enlightenResource'];

    function enlightenStats(enlightenResource) {

      var stats ={
        getStats: getStats
      };
      return stats;

      function getStats(systemId, start, end) {
        var params = {
          systemId: systemId
        };
        if (start !== null) {
          params.start = start;
          params.end = end;
        }

        return enlightenResource.query('/systems/:systemId/stats', params);
      }

    }

})();
