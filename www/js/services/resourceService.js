(function() {
  'use strict';

  angular
    .module('enlighten')
    .factory('enlightenResource', enlightenResource);

    enlightenResource.$inject = ['$resource', 'enlightenStorage'];

    function enlightenResource($resource, enlightenStorage) {

      var resource ={
        query: query
      };
      return resource;

      function query(url, params) {

        // set common parameters for all API calls
        params.key = enlightenStorage.getApiKey();
        params.callback = 'JSON_CALLBACK';

        var resource = $resource(enlightenStorage.getUrl() + url, {}, {
          jsonpquery: {
            method: 'JSONP',
            params: params,
            isArray: false
          }
        });
        return resource.jsonpquery();

      }

    }

})();