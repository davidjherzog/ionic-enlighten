(function() {
  'use strict';

  angular
    //.module('enlighten', ['ionic', 'enlighten.controllers'])
    .module('enlighten')
    .config(config);

    config.$inject = ['$httpProvider', '$stateProvider', '$urlRouterProvider'];

    function config($httpProvider, $stateProvider, $urlRouterProvider) {
      $stateProvider

        .state('app', {
          url: "/app",
          abstract: true,
          templateUrl: "templates/menu.html",
          controller: 'AppCtrl as menuView',
        })

        .state('app.details', {
          url: "/details",
          views: {
            'menuContent' :{
              templateUrl: "templates/details.html",
              controller: "DetailsCtrl as vm"
            }
          }
        })

        .state('app.help', {
          url: "/help",
          views: {
            'menuContent' :{
              templateUrl: "templates/help.html"
            }
          }
        })

        .state('app.preferences', {
          url: "/preferences",
          views: {
            'menuContent' :{
              templateUrl: "templates/preferences.html",
              controller: "PreferencesCtrl",
              controllerAs: "vm"
            }
          }
        });
      // if none of the above states are matched, use this as the fallback
      $urlRouterProvider.otherwise('/app/details');

      $httpProvider.defaults.headers.common['Accept'] = 'application/json';
      $httpProvider.defaults.headers.common['Content-Type'] = 'application/json';
    }

})();
