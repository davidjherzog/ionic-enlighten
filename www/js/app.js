// Ionic Starter App
(function() {
  'use strict';

  Date.prototype.toDateInputValue = (function() {
    var local = new Date(this);
    local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
    return local.toJSON().slice(0,10);
  });
  Date.prototype.addMonths = (function(months) {
    this.setMonth(this.getMonth() + months);
    return this;
  });
  console.log('Today\'s Date: ' + new Date().toDateInputValue());

  // angular.module is a global place for creating, registering and retrieving Angular modules
  // 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
  // the 2nd parameter is an array of 'requires'
  // 'starter.controllers' is found in controllers.js
  angular
    .module('enlighten', ['ionic', 'ngResource'])
    .run(function($ionicPlatform) {
      $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if(window.cordova && window.cordova.plugins.Keyboard) {
          cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if(window.StatusBar) {
          // org.apache.cordova.statusbar required
          StatusBar.styleDefault();
        }
      });
  });

  /*
   .factory('serviceInterceptor', ['$ionicLoading', function($ionicLoading) {
   var requestInterceptor = {
   request: function(config) {
   $ionicLoading.show({
   template: 'Loading...'
   });
   },
   response: function(config) {
   $ionicLoading.hide();
   }
   };

   return requestInterceptor;
   }]);
   */

})();
